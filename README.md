# collateral

Collateral is a property or an asset that a borrower offers as a way for a lender to secure the loan. Collateral arrangement is a risk reduction tool that mitigates risk by reducing credit exposure. Collateral doesn’t turn a bad counterparty into a g